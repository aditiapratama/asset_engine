class EngineDocument(object):
    """
    A Document object that our Engine can read/write
    Implements CRUD
    EngineDocument.
    """
    database = None
    def __init__(self):
        pass

    @classmethod
    def create(self):
        pass

    @classmethod
    def read(self):
        pass

    @classmethod
    def update(self):
        pass
    
    @classmethod
    def delete(self):
        pass


class EngineElement(object):
    def __init__(self):
        pass


class EngineCollection(object):
    def __init__(self):
        pass


class EngineContext(object):
    def __init__(self):
        pass


class EngineAsset(object):
    def __init__(self):
        pass


class EngineAssembly(object):
    def __init__(self):
        pass


class EngineProject(object):
    def __init__(self):
        pass


class EngineTask(object):
    def __init__(self):
        pass
        

class EngineTool(object):
    def __init__(self):
        pass
